#!/usr/bin/env bash

# 环境配置： 需要事先定义环境参数 export PROJ_PATH="项目根目录"

# 配置docker参数名称
export docker_image_name=iws
export docker_container_name=iws_cont

# 编译打包
cd $PROJ_PATH
mvn clean install -Dmaven.test.skip=true

# 清理docker环境
docker rm -f $docker_container_name
docker rmi $docker_image_name

# 构建新docker镜像
cd $PROJ_PATH/server/docker
rm -f *.jar
cp $PROJ_PATH/server/target/iWebService.jar .
cp $PROJ_PATH/server/lib/*.jar .
docker build -t $docker_image_name .

# 启动docker容器
docker run -d --name $docker_container_name -p 9000:9000 $docker_image_name
