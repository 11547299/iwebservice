package com.testcase.ws;

import com.test.service.WebService;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

public class TestWS {

    static WebService client = null;

    @BeforeAll
    public static void begin(){
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(WebService.class);
        factory.setAddress("http://localhost:9000/webService");
        client = (WebService) factory.create();
    }

    @Test
    public void testWeatherBeijing() {
        String reply = client.weather("bj");
        System.out.println("Response from Server : " + reply);
        Assertions.assertTrue(reply.toLowerCase().contains("beijing")
                , "The correct reply msg should contains beijing");
    }

    @Test
    public void testWeatherShangHai() {
        String reply = client.weather("sh");
        System.out.println("Response from Server : " + reply);
        Assertions.assertTrue(reply.toLowerCase().contains("shanghai")
                , "The correct reply msg should contains shanghai");
    }

    @Test
    public void testWeatherTianJin() {
        String reply = client.weather("tj");
        System.out.println("Response from Server : " + reply);
        Assertions.assertTrue(reply.toLowerCase().contains("tianjin")
                , "The correct reply msg should contains tianjin");
    }
}
