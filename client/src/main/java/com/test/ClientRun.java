package com.test;

import com.test.service.WebService;
import org.apache.cxf.jaxws.JaxWsProxyFactoryBean;


public class ClientRun {
    public static void main(String[] args) {
        JaxWsProxyFactoryBean factory = new JaxWsProxyFactoryBean();
        factory.setServiceClass(WebService.class);
        factory.setAddress("http://localhost:9000/webService");
        WebService client = (WebService) factory.create();
        String reply = client.weather("bj");
        System.out.println("Response from Server : " + reply);
    }
}
