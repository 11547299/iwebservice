package com.test;

import com.test.service.WebService;
import com.test.service.WebServiceImpl;
import org.apache.cxf.jaxws.JaxWsServerFactoryBean;

public class ServerRun {
    public static void main(String[] args) {
        String ipAddr = "0.0.0.0";
        WebServiceImpl implementor = new WebServiceImpl();
        JaxWsServerFactoryBean svrFactory = new JaxWsServerFactoryBean();
        svrFactory.setServiceClass(WebService.class);
        svrFactory.setAddress("http://" + ipAddr + ":9000/webService");
        svrFactory.setServiceBean(implementor);
        svrFactory.create();
        System.out.println("http://" + ipAddr + ":9000/webService?wsdl");
    }
}
