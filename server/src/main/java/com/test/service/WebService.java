package com.test.service;

@javax.jws.WebService
public interface WebService {
    String weather(String city);
}
