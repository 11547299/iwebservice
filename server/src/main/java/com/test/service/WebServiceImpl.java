package com.test.service;

public class WebServiceImpl implements WebService {
    @Override
    public String weather(String city) {
        String w = "";
        switch (city) {
            case "bj":
                w = "sunny";
                city = "BeiJing";
                break;
            case "tj":
                w = "windy";
                city = "TianJin";
                break;
            case "sh":
                w = "raining";
                city = "ShangHai";
                break;
            default:
                w = "error input";
        }

        String rt = "The weather of " + city + " is " + w + ".";
        System.out.println(rt);
        return rt;
    }
}
